// JavaScript File
angular.module('appRoutes',['ngRoute']).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
    
    $routeProvider
    //home page
    
    .when('/entry', {
        templateUrl: 'views/entry.html',
        controller: 'EntryController'
        
    })
    
    .when('/bear',{
        templateUrl: 'views/bear.html',
        controller: 'BearController'
        
    })
    
    .when('/entry',{
        templateUrl: 'views/entry.html',
        controller: 'EntryController'
        
    });
    //.when('/',{
      //  templateUrl: 'views/index.html',
      //  controller: 'MainController'
    //});
    //.otherwise({
   //    redirectTo: '/'
   // });
    
    $locationProvider.html5Mode(true);
    
}]);