// JavaScript File

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PostSchema   = new Schema({
    name: {type: String, required: true},
    style: { type: String},
    email: {type: String}
    //data: String,
});

module.exports = mongoose.model('Post', PostSchema);