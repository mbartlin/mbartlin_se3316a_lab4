// JavaScript File

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    token: {type: String, required: true},
    name: { type: String},
    email: { type: String},
    currentU: {type: Boolean}
    //data: String,
});

module.exports = mongoose.model('User', UserSchema);