var application_root = __dirname;
var express = require('express');
var bodyparser = require('body-parser');
var http = require('http');
var app = express();
var path = require("path");
var morgan = require('morgan');
var mongoose = require('mongoose');
var port = process.env.PORT;
//var cache = require("cache-parser");
var router = express.Router();

//app.use(cache());
mongoose.connect('mongodb://'+process.env.IP+':27017');

app.use(morgan('dev'));
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use(express.static(__dirname + "/public"));
app.use('/api', router);

app.use('/public',express.static(path.resolve(__dirname + "s/public")));
app.use('/',express.static(path.resolve(__dirname + "/public")));
app.get('/*',function(req,res){
  res.sendfile(path.join(__dirname+'/public/index.html'));
  //__dirname : It will resolve to your project folder.
});

var Post = require('./models/posts');
var User = require('./models/users');

/*Schemas
var PostSchema= new Schema({
    title: {type: String, required: true},
    description: { type: String, required: true},
    style: { type: String, unique: true },
    //modified: { type: Date, default: Date.now }
});

var contactSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true},
    number: {type: String, required: true}
});

var PostModel = mongoose.model('Post', PostSchema);
var ContactModel = mongoose.model('Contact', contactSchema);
//Schemas end */


router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});



// router.get('/posts', function(req, res) {
    
//     //    Post.find(function(err, posts) {
    
//     Post.find({email :req.body.email},function(err, posts) {
//               if (err)
//             {
//                 res.send(err);
//             }

//             res.json(posts);
//  });

// });
router.post('/postsGet', function(req, res) {
    
    //    Post.find(function(err, posts) {
    
    Post.find({email :req.body.email},function(err, posts) {
               if (err)
            {
                res.send(err);
            }

            res.json(posts);
 });

});
 
 router.get('/users', function(req, res) {
        User.find(function(err, users) {
            if (err)
            {
                res.send(err);
            }

            res.json(users);
 });

});

router.post('/posts', function(req, res) {
        
        var post = new Post();      // create a new instance of the Post model
        post.name = req.body.name;  // set the bears name (comes from the request)
        post.email = req.body.email;
       // post.data = req.body.data;
       post.style = req.body.style;
        // save the bear and check for errors
        post.save(function(err) {
            if (err)
            {
                console.log(err);
                res.send(err);
            }
            else
            {
                res.send('Post created!');
            }
            
            
        });
        //res.json({ message: 'Post created!' });
        
    });
    
    router.post('/users', function(req, res) {
        
        var user = new User();      // create a new instance of the Post model
        user.token = req.body.token;  // set the bears name (comes from the request)
       user.name = req.body.name;
       user.email = req.body.email;
       user.currentU = req.body.currentU;
        
        user.save(function(err) {
            if (err)
            {
                console.log(err);
                res.send(err);
            }
            else
            {
                res.send('User created!');
            }
            
            
        });
        //res.json({ message: 'Post created!' });
        
    });
    
    
router.get('/posts/:post_id', function(req, res) {
        Post.findById(req.params.post_id, function(err, post) {
            if (err)
            {
                res.send(err);
            }
            res.json(post);
        });
    });    
    
    
router.put('/posts/:post_id', function(req, res) {

        // use our bear model to find the bear we want
        Post.findById(req.params.post_id, function(err, post) {

            if (err)
            {
                res.send(err);
            }

            post.name = req.body.name;  // update the bears info

            // save the bear
            post.save(function(err) {
                if (err)
                {
                    res.send(err);
                }

                res.json({ message: 'Post updated!' });
            });

        });
    });
    
    
router.delete('/posts/:post_id', function(req, res) {
        Post.remove({
            _id: req.params.post_id
        }, function(err, bear) {
            if (err)
            {
                res.send(err);
            }

            res.json({ message: 'Successfully deleted post' });
        });
    });
    
router.get('*', function(req, res) {
   res.redirect('/'); 
});

/* POSTS functions
app.get('/api/posts',function(req, res){
    
    return PostModel.find(function(err, posts){
        if(!err)
        {
            return res.send(posts);
        }
        else{
            return console.log(err);
        }
        
    });
    
}); 



app.post('/api/posts', function(request,response){
    
    var contact;
    console.log("POST: ");
    console.log(request.body);
    contact = new ContactModel({
        name: request.body.name,
        email: request.body.email,
        number: request.body.number
    });
    
    contact.save(function(err){
        if(!err)
        {
            return console.log("created");
        }
        else
        {
            return console.log(err);
        }
        
    });
    
    return response.send(contact);
}); 


app.get('/api/posts/:id', function(req,res){
    
    return PostModel.findById(req.params.id,function(err, post){
        
        if(!err)
        {
            return res.send(post);
        }
        else
        {
            return console.log(err);
        }
    });
    
}); 

app.put('/api/posts/:id', function(req,res){
    
    return PostModel.findById(req.params.id,function(err, post){
        
        post.title = req.body.title;
        post.description = req.body.description;
        post.style = req.body.style;
        
        return post.save(function(err){
            if(!err)
            {
                console.log("updated");
            }
            else
            {
                console.log(err);
            }
            
            return res.send(post);
            
        });
        
    });
    
});


app.delete('/api/posts/:id', function(req, res){
    return PostModel.findById(req.params.id, function(err, product){
        
        return product.remove(function(err){
            if(!err){
                console.log("removed");
                return res.send('');
            }
            
            else
            {
                console.log(err);
            }
            
            
        });
        
    });
    
}); 

*/

/* CONTACT LIST TUTORIAL POST AND GET
app.post('/api/posts', function(req,res){
    
    var post;
    console.log("POST: ");
    console.log(req.body);
    post = new PostModel({
        title: req.body.title,
        description: req.body.description,
        style: req.body.style,
        
    });
    
    post.save(function(err){
        if(!err)
        {
            return console.log("created");
        }
        else
        {
            return console.log(err);
        }
        
    });
    
    return res.send(post);
    
}); 

app.get('/api/posts',function(request, response){
    console.log("I received a get request")
    
    var person1 = {
    name: 'Emily2',
    email: 'emily2@gmail.com',
    number: '1111'
    };

    var person2 = {
    name: 'Emily3',
    email: 'emily3@gmail.com',
    number: '11112'
    };

    var contactlist = [person1, person2];

    response.json(contactlist);

}); 


END OF CONTACT LIST TUTORIAL GET AND POST*/



var server = http.createServer(app);
//console.log('Magic happens on port ' + port);
//app.listen(process.env.IP,port);
server.listen(process.env.PORT);